

GO_VERSION=1.5.3
export GO_VERSION

GOOS=freebsd
export GOOS

CGO_ENABLED=0
export CGO_ENABLED

GOARCH=amd64
export GOARCH
GO_ROOT_INSTALL=$HOME/proglang/golang/goroot$GO_VERSION
export GO_ROOT_INSTALL

GOROOT_BOOTSTRAP=$GO_ROOT_INSTALL/go1.4
export GOROOT_BOOTSTRAP

GOROOT=$GO_ROOT_INSTALL/gosrc/go
export GOROOT

GOPATH=$GO_ROOT_INSTALL/go
export GOPATH

#Adding GO BIN to my path.
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/games:/usr/local/sbin:/usr/local/bin:/usr/local/jenkins/bin:./:$GOROOT/bin:$GOPATH/bin
export path

MORE="-erX" ; export MORE
CLICOLOR=true ; export CLICOLOR

